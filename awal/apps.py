from django.apps import AppConfig


class AwalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'awal'
