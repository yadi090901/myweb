from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('awal/', include('awal.urls')),
    path('tengah/', include('tengah.urls')),
    path('akhir/', include('akhir.urls')),
]
